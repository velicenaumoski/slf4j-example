package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(Application.class.getName());
    System.out.println("This is system out print line and we don't like it!");

    logger.info("Useful events that tell us what is happening in the system. "
        + "Typically events used to monitor analytics and system health.");
    logger.warn("We handled an error and we are able to recover from it and "
        + "continue application flow in an expected manner.");
    logger.error("An error we could not recover from and the application "
        + "flow is ending unexpectedly.");

    logger.debug("More detailed events used in testing environments to help "
        + "with debugging features about to go into production.");

    logger.trace("Finely detailed events that should only ever be used on "
        + "developer machines while developing a feature.");
  }
}
